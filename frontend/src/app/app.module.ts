import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {SigninViewComponent} from './views/signin.component';
import {AuthenticationService} from './services/authentication.service';
import {SignupViewComponent} from './views/signup.component';
import {DashboardViewComponent} from './views/dashboard.component';
import {AuthGuardService} from './services/authentication-guard.service';
import {ImageComponent} from './components/image.component';
import {NavbarComponent} from './components/navbar.component';
import {UserService} from './services/user.service';
import {ImageUploadComponent} from './components/image-upload.component';
import {NgMasonryGridModule} from 'ng-masonry-grid';
import {ForgotPasswordComponent} from './views/forgotpassword.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
    SigninViewComponent,
    SignupViewComponent,
    DashboardViewComponent,
    ImageComponent,
    NavbarComponent,
    ImageUploadComponent,
    ForgotPasswordComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    NgMasonryGridModule,
    FontAwesomeModule,
    ReactiveFormsModule
  ],
  providers: [
    AuthenticationService,
    AuthGuardService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
