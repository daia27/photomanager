import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninViewComponent } from './views/signin.component';
import {SignupViewComponent} from './views/signup.component';
import {DashboardViewComponent} from './views/dashboard.component';
import {AuthGuardService} from './services/authentication-guard.service';
import {ForgotPasswordComponent} from './views/forgotpassword.component';

const routes: Routes = [
  { path: '', redirectTo: 'signin', pathMatch: 'full' },
  { path: 'signin', component: SigninViewComponent },
  { path: 'forgotpassword', component: ForgotPasswordComponent },
  { path: 'signup', component: SignupViewComponent },
  { path: 'dashboard', component: DashboardViewComponent, canActivate: [AuthGuardService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
