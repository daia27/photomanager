import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class UserService {
  change: Subject<any> = new Subject<any>();

  constructor() {}

  /**
   * Use to change user name
   * @data type: string
   */
  setUser(data: string) {
    this.change.next(data);
  }
}
