import {Component, OnInit} from '@angular/core';
import { AuthenticationService, TokenPayload } from '../services/authentication.service';
import { Router } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";


@Component({
  selector: 'app-signup-view',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupViewComponent implements OnInit {
  credentialsForm: FormGroup;
  submitted: boolean;

  constructor(private auth: AuthenticationService, private router: Router, private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.credentialsForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      name: ['', [Validators.required]]
    });
  }

  signup() {
    this.submitted = true;

    if (this.credentialsForm.invalid) {
      return;
    }

    this.auth.signup({
      email: this.credentialsForm.get('email').value,
      password: this.credentialsForm.get('password').value,
      name: this.credentialsForm.get('name').value,
    }).subscribe(() => {
      this.router.navigateByUrl('/dashboard');
    }, (err) => {
      console.error(err);
    });
  }
}
