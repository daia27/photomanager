import {Component, OnInit} from '@angular/core';
import { AuthenticationService, TokenPayload } from '../services/authentication.service';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-signin-view',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninViewComponent implements OnInit {
  credentialsForm: FormGroup;
  submitted: boolean;
  error: string;

  constructor(private auth: AuthenticationService, private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.credentialsForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }

  signin() {
    this.error = '';
    this.submitted = true;

    if (this.credentialsForm.invalid) {
      return;
    }

    this.auth.signin({
      email: this.credentialsForm.get('email').value,
      password: this.credentialsForm.get('password').value,
    }).subscribe(() => {
      this.router.navigateByUrl('/dashboard');
    }, (err) => {
      console.error(err);

      if (err.status === 401) {
        this.error = 'Invalid email or password.';
      } else {
        this.error = err.error.message;
      }
    });
  }
}
