import {Component, OnInit} from '@angular/core';
import {AuthenticationService, TokenPayload, UserDetails} from '../services/authentication.service';
import { Router } from '@angular/router';
import {HttpClient, HttpParams} from '@angular/common/http';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardViewComponent implements OnInit {
  user: UserDetails;
  images: any;

  constructor(private auth: AuthenticationService, private http: HttpClient, private userService: UserService) {}

  ngOnInit() {
    this.auth.user().subscribe(user => {
      this.user = user;
      this.userService.setUser(user);
      this.getImages();
    }, (err) => {
      console.error(err);
    });
  }

  getImages() {
    const params = new HttpParams().set('userID', this.user._id);
    this.http.get('http://138.68.80.65/api/images', {params})
      .subscribe(res => this.images = res);
  }
}
