import {Component, OnInit} from '@angular/core';
import {AuthenticationService, UserDetails} from '../services/authentication.service';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit{
  user: UserDetails;
  isNavbarCollapsed=true;

  constructor(private auth: AuthenticationService, private userService: UserService) {}

  ngOnInit() {
    this.userService.change.subscribe((user) => {
      this.user = user;
    });
  }

  signout() {
    this.userService.setUser(null);
    this.auth.signout();
  }
}
