import {Component, Input, OnInit} from '@angular/core';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';
import {UserDetails} from '../services/authentication.service';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-image-upload-modal',
  templateUrl: './image-upload.component.html'
})
export class ImageUploadComponent implements OnInit{
  user: UserDetails;
  closeResult: string;
  file: File;
  url: string;
  uploading: boolean;
  @Input() images: any;
  info = {
    location: '',
    date: '',
    tags: '',
    description: ''
  };

  constructor(private modalService: NgbModal, private http: HttpClient, private userService: UserService) {}

  ngOnInit() {
    this.userService.change.subscribe((user) => {
      this.user = user;
    });
  }

  onSelectedImage(event) {
    this.file = <File>event.target.files[0];
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(this.file);
    reader.onload = (e: Event) => {
      this.url = reader.result as string;
    };
  }

  onUpload(event) {
    const formData = new FormData();
    formData.append('file', this.file, this.file.name );
    formData.append('userID', this.user._id);
    formData.append('location', this.info.location);
    formData.append('description', this.info.description);
    formData.append('date', this.info.date);
    formData.append('tags', this.info.tags);

    this.uploading = true;
    this.http.post('http://138.68.80.65/api/images', formData)
      .subscribe(res => {
        this.images.push(res);
        this.close();
      });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  private close() {
    this.modalService.dismissAll('Uploaded');
    this.file = null;
    this.url = '';
    this.info = {
      location: '',
      date: '',
      tags: '',
      description: ''
    };
    this.uploading = false;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
