import {Component, Input, OnInit} from '@angular/core';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { faHeart as faHeartSolid, faPaperPlane, faTrash } from '@fortawesome/free-solid-svg-icons';
import { faHeart as faHeartOutline } from '@fortawesome/free-regular-svg-icons';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent {
  @Input() data;
  @Input() images;
  closeResult: string;
  favorite: boolean;
  commentDraft = '';

  faHeartSolid = faHeartSolid;
  faHeartOutline = faHeartOutline;
  faPaperPlane = faPaperPlane;
  faTrash = faTrash;

  constructor(private modalService: NgbModal, private http: HttpClient) {}

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  private markFavorite(event) {
    this.data.favorite = !this.data.favorite;

    this.http.post('http://138.68.80.65/api/favorite', {
        id: this.data._id,
        favorite: this.data.favorite
      })
      .subscribe(res => console.log(res));

    event.stopPropagation();
  }

  addComment() {
    if (this.commentDraft !== '') {
      this.data.comments.push(this.commentDraft);
      this.commentDraft = '';

      this.http.post('http://138.68.80.65/api/comment', {
          id: this.data._id,
          comments: this.data.comments
        })
        .subscribe(res => console.log(res));
    }
  }

  delete() {
    this.http.delete('http://138.68.80.65/api/images', {
        params: {
          id: this.data._id,
        }
      })
      .subscribe((res: any) => {
        if (res.success) {
          const index = this.images.map((img) => img._id).indexOf(this.data._id);
          this.images.splice(index, 1);
          this.modalService.dismissAll('Delete');
        }
      });
  }
}

