const mongoose = require( 'mongoose' );

const imageSchema = new mongoose.Schema({
    userID: {
        type: String,
        unique: false,
        required: true
    },

    tags: {
        type: [String]
    },

    comments: {
        type: [String]
    },

    url: {
        type: String,
        unique: true,
        required: true
    },

    cloudinaryID: {
        type: String,
        unique: true,
        required: true
    },

    description: {
        type: String,
        unique: false,
        required: false
    },

    location: {
        type: String,
        unique: false,
        required: false
    },

    date: {
        type: Date,
        unique: false,
        required: false
    },

    favorite: {
        type: Boolean,
        unique: false,
        required: false
    }
});

mongoose.model('Image', imageSchema);