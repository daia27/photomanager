const express = require('express');
const router = express.Router();
const jwt = require('express-jwt');
const auth = jwt({
    secret: 'MY_SECRET',
    userProperty: 'payload'
});

const ctrlUser = require('./controllers/user');
const ctrlAuth = require('./controllers/authentication');
const ctrlUpload = require('./controllers/imageupload');


// profile
router.get('/user', auth, ctrlUser.userRead);

// authentication
router.post('/signup', ctrlAuth.signup);
router.post('/signin', ctrlAuth.signin);

//image upload
router.get('/images', ctrlUpload.imageRead);
router.post('/images', ctrlUpload.parser.single("file"), ctrlUpload.uploadImage);
router.delete('/images', ctrlUpload.imageDelete);
router.post('/favorite', ctrlUpload.favorite);
router.post('/comment', ctrlUpload.comment);

module.exports = router;