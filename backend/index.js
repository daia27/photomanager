const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');

// Data model
require('./models/db');

// Passport configuration
// Needs to be after defining the data model
require('./config/passport');

// Routes for the API (delete the default routes)
const apiRoutes = require('./routes');

// Initialize express
const app = express();

//app.use(favicon(__dirname + '/public/favicon.ico')); // uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

// Set default view engine
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// Initialise Passport
// Needs to be done before using the route middleware
app.use(passport.initialize());

// Routes
app.use('/api', apiRoutes);

const router = express.Router();

// Serve Angular files
app.use(express.static(path.join(__dirname, "../frontend/dist/frontend")));
app.use('*', function(req, res) {
    res.sendfile('index.html');
});

// Ccatch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;

    next(err);
});

// Error handlers

// Catch unauthorised errors
app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401);
        res.json({"message" : err.name + ": " + err.message});
    }

    next(err);
});

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: app.get('env') === 'development' ? err : {}
    });
});


// app.set('port', process.env.PORT || 8000);
app.listen(process.env.PORT || 3000);

module.exports = app;
