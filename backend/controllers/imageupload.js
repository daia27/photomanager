const multer = require("multer");
const cloudinary = require("cloudinary");
const cloudinaryStorage = require("multer-storage-cloudinary");
const mongoose = require( 'mongoose' );
const Image = mongoose.model('Image');

cloudinary.config({
    cloud_name: 'drpvnpvwr',
    api_key: '439986268916266',
    api_secret: 'mWBYm-46meUwWXiKbl87F6V9q0E'
});
const storage = cloudinaryStorage({
    cloudinary: cloudinary,
    folder: "demo",
    allowedFormats: ["jpg", "png"],
    transformation: [{ width: 1280, height: 1280, crop: "limit" }]
});
const parser = multer({ storage: storage });

module.exports.parser = parser;

module.exports.uploadImage = function(req, res){
    const image = {};
    image.userID = req.body.userID;
    image.url = req.file.url;
    image.cloudinaryID = req.file.public_id;
    image.location = req.body.location;
    image.description = req.body.description;
    image.date = req.body.date || (new Date()).toString();
    image.tags = req.body.tags.split(/\s+/g);
    Image.create(image) // save image information in database
        .then(newImage => res.json(newImage))
        .catch(err => console.log(err));
};

module.exports.imageRead = function (req, res) {
    Image.find({userID: req.query.userID})
        .then(images => res.json(images))
        .catch(err => console.log(err));
};


module.exports.imageDelete = function (req, res) {
    Image.deleteOne({ _id: req.query.id }, function(err){
        if (err) { throw err; }
        res.json({ success: true })
    });
};

module.exports.favorite = function (req, res) {
    Image.updateOne({ _id: req.body.id }, { favorite: req.body.favorite }, { upsert: true }, function(err){
        if (err) { throw err; }

        res.json({ success: true })
    });
};

module.exports.comment = function (req, res) {
    Image.updateOne({ _id: req.body.id }, { comments: req.body.comments }, { upsert: true }, function(err){
        if (err) { throw err; }

        res.json({ success: true })
    });
};