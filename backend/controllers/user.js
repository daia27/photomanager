const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports.userRead = function(req, res) {
    if (!req.payload._id) {
        res.status(401).json({
            "message" : "UnauthorizedError: private profile"
        });
    } else {
        User
            .findOne({ _id: req.payload._id }, (err, user) => {
                console.log(err, user)
                res.status(200).json(user);
            })
    }
};